#!/usr/bin/env bash

export LIBGL_ALWAYS_INDIRECT=0

alias ssh='/mnt/c/Windows/System32/OpenSSH/ssh.exe'
alias ssh-add='/mnt/c/Windows/System32/OpenSSH/ssh-add.exe'
alias ssh-keygen='/mnt/c/Windows/System32/OpenSSH/ssh-keygen.exe'
alias gpg='/mnt/c/Program\ Files\ \(x86\)/GnuPG/bin/gpg.exe'

export LOADED_WSLSH=1